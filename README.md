# ROBOTIC VACUUM CLEANER README #

This README explains what this repository contains.

The folder **VacuumApp** is the Qt Widget project folder for the application of the Robot.

The other folder **vacuum_cleaner** contains the source code for the Arduino to control the robot directly.

# Contributors #

s3461695 - Nguyen Thi Bao Chau

s3464688 - Nguyen Ngoc Ha     

s3461484 - Bui Minh Dung      

s3463827 - Doan Thien Phuc    