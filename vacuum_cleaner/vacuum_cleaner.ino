#include <NewPing.h>
#include <SoftwareSerial.h>  

// constants for Arduino pins
#define MAX_DISTANCE 300
#define ANGLE        0.9                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
#define DEBUG true

#define TRIGGER_FRONT 10
#define ECHO_FRONT 14
#define TRIGGER_LEFT 12
#define ECHO_LEFT 16
#define TRIGGER_RIGHT 8
#define ECHO_RIGHT 17

// constants for codes used for communication with the application
#define TURN_LEFT 1
#define TURN_RIGHT 2
#define MOVE_FORWARD 3
#define MOVE_BACKWARD 4
#define STOP_MOVING 5
#define START_ALGORITHM 6

bool obstacle_mode = false; // boolean set true if the robot is contouring an obstacle
int count = 0;

// Motor 1
int left1 = 2;
int left2 = 3;
int speedLeft = 6;

// Motor 2
int right1 = 4;
int right2 = 5;
int speedRight = 9;

// variables to store values sensed by sensors
long frontDist = 0;
long leftDist1 = 0;
long leftDist2 = 0;
long rightDist1 = 0;
long rightDist2 = 0;

int fanPin = 13;

// booleans to check whether the robot has reached the end of the room
bool stop1 = false;
bool stop2 = true;

SoftwareSerial esp8266(7,11); // (TX, RX)

NewPing frontS(TRIGGER_FRONT, ECHO_FRONT, MAX_DISTANCE);
NewPing rightS(TRIGGER_RIGHT, ECHO_RIGHT, MAX_DISTANCE);
NewPing leftS(TRIGGER_LEFT, ECHO_LEFT, MAX_DISTANCE);

void setup() {  // Setup runs once per reset
// initialize serial communication @ 9600 baud:
Serial.begin(9600);

esp8266.begin(9600);

//Define L298N Dual H-Bridge Motor Controller Pins
pinMode(left1, OUTPUT);
pinMode(left2, OUTPUT);
pinMode(right1, OUTPUT);
pinMode(right2, OUTPUT);
pinMode(fanPin, OUTPUT);

pinMode(speedLeft, OUTPUT);
pinMode(speedRight, OUTPUT);

analogWrite(speedLeft, 99);
analogWrite(speedRight, 55);

// commands to set up ESP server
sendData("AT+RST\r\n", 2000, DEBUG); // reset module
sendData("AT+CWMODE=2\r\n", 1000, DEBUG); // configure as access point
sendData("AT+CIFSR\r\n", 1000, DEBUG); // get ip address
sendData("AT+CIPMUX=1\r\n", 1000, DEBUG); // configure for multiple connections
sendData("AT+CIPSERVER=1,6543\r\n", 1000, DEBUG); // turn on server on port 80

}

void control_fan() {
  digitalWrite(fanPin, HIGH);
  delay(5000);
  digitalWrite(fanPin, LOW);
  delay(5000);
}


void sense(char side) {
  if (side == 'F') { // use front sensor
    
    int tries = 0;
    unsigned int uSf;
    
    do {
      uSf = frontS.ping(); // Send ping, get ping time in microseconds (uS).
      frontDist = frontS.convert_cm(uSf);
      if (frontDist ==0) {
        delay(70); 
        pinMode(ECHO_FRONT, OUTPUT);
        digitalWrite(ECHO_FRONT, LOW);
        delay(70);
        pinMode(ECHO_FRONT, INPUT);
      }
    } while (frontDist == 0 && ++tries <3);
    Serial.print("Ping Front: ");
    Serial.print(frontDist); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.println("cm");
  }

  else if (side == 'L') { // use left sensor
    int tries = 0;
    unsigned int uSl1;
    
    do {
      uSl1 = leftS.ping(); // Send ping, get ping time in microseconds (uS).
      leftDist1 = leftS.convert_cm(uSl1);
      if (leftDist1 ==0) {
        delay(70); 
        pinMode(ECHO_LEFT, OUTPUT);
        digitalWrite(ECHO_LEFT, LOW);
        delay(70);
        pinMode(ECHO_LEFT, INPUT);
      }
    } while (leftDist1 == 0 && ++tries <3);
    Serial.print("Ping Left 1: ");
    Serial.print(leftDist1); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.print("cm \t");
    
    move_forward(0.5);

    tries = 0;
    unsigned int uSl2;
    
    do {
      uSl2 = leftS.ping(); // Send ping, get ping time in microseconds (uS).
      leftDist2 = leftS.convert_cm(uSl2);
      if (leftDist2 ==0) {
        delay(70); 
        pinMode(ECHO_LEFT, OUTPUT);
        digitalWrite(ECHO_LEFT, LOW);
        delay(70);
        pinMode(ECHO_LEFT, INPUT);
      }
    } while (leftDist2 == 0 && ++tries <3);
    Serial.print("Ping Left 2: ");
    Serial.print(leftDist2); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.println("cm");
    
  }
  else if (side == 'R') { // use right sensor
    int tries = 0;
    unsigned int uSr1;
    
    do {
      uSr1 = rightS.ping(); // Send ping, get ping time in microseconds (uS).
      rightDist1 = rightS.convert_cm(uSr1);
      if (rightDist1 ==0) {
        delay(70); 
        pinMode(ECHO_RIGHT, OUTPUT);
        digitalWrite(ECHO_RIGHT, LOW);
        delay(70);
        pinMode(ECHO_RIGHT, INPUT);
      }
    } while (rightDist1 == 0 && ++tries <3);
    Serial.print("Ping Right 1: ");
    Serial.print(rightDist1); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.print("cm \t");
    
    move_forward(0.5);

    tries = 0;
    unsigned int uSr2;
    
    do {
      uSr2 = rightS.ping(); // Send ping, get ping time in microseconds (uS).
      rightDist2 = rightS.convert_cm(uSr2);
      if (rightDist2 ==0) {
        delay(70); 
        pinMode(ECHO_RIGHT, OUTPUT);
        digitalWrite(ECHO_RIGHT, LOW);
        delay(70);
        pinMode(ECHO_RIGHT, INPUT);
      }
    } while (rightDist2 == 0 && ++tries <3);
    Serial.print("Ping Right 2: ");
    Serial.print(rightDist2); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.println("cm");
  }

  else if (side == 'A') { // use all 3 sensors but only get 1 value for each side instead of 2
    Serial.println("Sense all");
    int tries = 0;
    unsigned int uSf;
    
    do {
      uSf = frontS.ping(); // Send ping, get ping time in microseconds (uS).
      frontDist = frontS.convert_cm(uSf);
      if (frontDist ==0) {
        delay(70); 
        pinMode(ECHO_FRONT, OUTPUT);
        digitalWrite(ECHO_FRONT, LOW);
        delay(70);
        pinMode(ECHO_FRONT, INPUT);
      }
    } while (frontDist == 0 && ++tries <3);
    Serial.print("Ping Front: ");
    Serial.print(frontDist); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.print("cm \n");
    
    tries = 0;
    unsigned int uSl1;
    
    do {
      uSl1 = leftS.ping(); // Send ping, get ping time in microseconds (uS).
      leftDist1 = leftS.convert_cm(uSl1);
      if (leftDist1 ==0) {
        delay(70); 
        pinMode(ECHO_LEFT, OUTPUT);
        digitalWrite(ECHO_LEFT, LOW);
        delay(70);
        pinMode(ECHO_LEFT, INPUT);
      }
    } while (leftDist1 == 0 && ++tries <3);
    Serial.print("Ping Left 1: ");
    Serial.print(leftDist1); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.print("cm \t");
    
    tries = 0;
    unsigned int uSr1;
    
    do {
      uSr1 = rightS.ping(); // Send ping, get ping time in microseconds (uS).
      rightDist1 = rightS.convert_cm(uSr1);
      if (rightDist1 ==0) {
        delay(70); 
        pinMode(ECHO_RIGHT, OUTPUT);
        digitalWrite(ECHO_RIGHT, LOW);
        delay(70);
        pinMode(ECHO_RIGHT, INPUT);
      }
    }  while (rightDist1 == 0 && ++tries <3);
    Serial.print("Ping Right 1: ");
    Serial.print(rightDist1); // Convert ping time to distance and print result (0 = outside set distance range, no ping echo)
    Serial.println("cm");
    
  }
}

void turn_left(double amount) { // turn an amount of time
  Serial.print("Turn left ");
  Serial.println(amount);
  analogWrite(speedLeft,155);
  analogWrite(speedRight,155);
  digitalWrite(right1, HIGH); // right wheel move forward
  digitalWrite(right2, LOW);
  digitalWrite(left1, LOW); // left wheel move backward
  digitalWrite(left2, HIGH);
  delay(amount*1000);
  analogWrite(speedLeft, 99);
  analogWrite(speedRight, 55);
}

void turn_right(double amount) {
  Serial.print("Turn right ");
  Serial.println(amount);
  analogWrite(speedLeft,155);
  analogWrite(speedRight,155);
  digitalWrite(left1, HIGH); // left wheel move forward
  digitalWrite(left2, LOW);
  digitalWrite(right1, LOW); // right wheel move backward
  digitalWrite(right2, HIGH);
  delay((amount)*1000);
  analogWrite(speedLeft, 99);
  analogWrite(speedRight, 55);
}

void align_left(bool compensate) { // align with the left obstacle
  Serial.println("Align Left");
  sense('L'); // sense for 2 left values
  if (compensate) move_backward(0.5); // if compensate is true, move backward to make up for the distance moved forward when sensing the 2 values
  while (leftDist1!=leftDist2) { // make sure the 2 distances are not different
    
    if (leftDist1 > leftDist2) {
      double amount = (leftDist1-leftDist2)/18.0;
      if (amount <= ANGLE) {
        turn_right(amount);
        sense('L');
        if (compensate) move_backward(0.5);
      }  else {
        sense('L');
        if (compensate) move_backward(0.5);
      }
    } else if (leftDist1 < leftDist2) {
      double amount = (leftDist2-leftDist1)/18.0;
      if (amount <= ANGLE) {
        turn_left(amount);
        sense('L');
        if (compensate) move_backward(0.5);
      }  else {
        sense('L');
        if (compensate) move_backward(0.5);
      }
    }
  }
}

void align_right(bool compensate) { // similar mechanism to align right
  Serial.println("Align Right");
  sense('R');
  if (compensate) move_backward(0.5);
  while (rightDist1!=rightDist2) { // make sure the 2 distances are not more than 2cm different
    
     if (rightDist1 > rightDist2) {
      double amount = (rightDist1-rightDist2)/18.0;
      if (amount <= ANGLE) {
        turn_left(amount);
        sense('R');
        if (compensate) move_backward(0.5);
      }  else {
        sense('R');
        if (compensate) move_backward(0.5);
      }
    } else if (rightDist1 < rightDist2) {
      double amount = (rightDist2-rightDist1)/18.0;
      if (amount <= ANGLE) {
        turn_right(amount);
        sense('R');
        if (compensate) move_backward(0.5);
      }  else {
        sense('R');
        if (compensate) move_backward(0.5);
      }
    }
  }
}

void move_forward(double amount) {
  Serial.print("Forward");
  Serial.println(amount);
  digitalWrite(left1, HIGH);
  digitalWrite(left2, LOW);
  digitalWrite(right1, HIGH);
  digitalWrite(right2, LOW);
  delay(amount*1000);
}

void move_backward(double amount) {
  Serial.print("Backward");
  Serial.println(amount);
  digitalWrite(left1, LOW);
  digitalWrite(left2, HIGH);
  digitalWrite(right1, LOW);
  digitalWrite(right2, HIGH);
  delay(amount*1000);
}

void stop_moving(double amount) {
  Serial.println("Stop Moving");
  digitalWrite(left1, LOW);
  digitalWrite(left2, LOW);
  digitalWrite(right1, LOW);
  digitalWrite(right2, LOW);
  delay(amount*1000);
}

void forward() {
  digitalWrite(left1, HIGH);
  digitalWrite(left2, LOW);
  digitalWrite(right1, HIGH);
  digitalWrite(right2, LOW);
}

void backward() {
  digitalWrite(left1, LOW);
  digitalWrite(left2, HIGH);
  digitalWrite(right1, LOW);
  digitalWrite(right2, HIGH);
}

void stopie() {
  digitalWrite(left1, LOW);
  digitalWrite(left2, LOW);
  digitalWrite(right1, LOW);
  digitalWrite(right2, LOW);
}

void left() {
  analogWrite(speedLeft,180);
  analogWrite(speedRight,180);
  digitalWrite(right1, HIGH); // right wheel move forward
  digitalWrite(right2, LOW);
  digitalWrite(left1, LOW); // left wheel move backward
  digitalWrite(left2, HIGH);
  analogWrite(speedLeft, 104);
  analogWrite(speedRight, 70);
}

void right() {
  analogWrite(speedLeft,180);
  analogWrite(speedRight,180);
  digitalWrite(left1, HIGH); // left wheel move forward
  digitalWrite(left2, LOW);
  digitalWrite(right1, LOW); // right wheel move backward
  digitalWrite(right2, HIGH);
  analogWrite(speedLeft, 104);
  analogWrite(speedRight, 70);
}

void compare_align(bool compensate1, bool compensate2) { // check to see which side is closer, then align with that side
  Serial.println("Compare align");
  sense('A');
  if (leftDist1 >= rightDist1 || leftDist1==0) align_right(compensate2);
  else if (leftDist1 < rightDist1 || rightDist1==0) align_left(compensate2);
}

void coverage_algorithm() {
  sense('A');
  if (leftDist1 < 30) {
    while (leftDist1 < 30) {
    LEFT:  
      align_left(true);
      
      if (!obstacle_mode) { 
        check_too_close('L', true);
      }
      sense('F');
      for(int i = 0; i<20; i++) {
        if (obstacle_mode) {
          Serial.println("Going first edge ");
          sense('A');
          while (frontDist>20 && (leftDist1<15 || leftDist1==0)) { // going along first edge of left obstacle
            move_forward(0.1);
            sense('A');
          }
          break; // end of first edge
        }
        else if (frontDist>20) // not obstacle mode
          move_forward(0.1);
        else break;
        sense('F');
      }
      if (frontDist < 20) { // meet front wall
        stop_moving(0.5);
        turn_right(ANGLE);
        align_left(true);
        move_forward(0.3);
        check_too_close('L', true);
        move_forward(1);
        
        goto AFTER_RIGHT;
      }
    }
    // went one side of obstacle, move to the other side
    move_forward(0.2);
    turn_left(ANGLE);
    move_forward(0.5);
    align_left(true);
    sense('A');
    while (leftDist1 < 15 && frontDist > 20) { // keep going along second edge
      Serial.println("Second edge ");
      move_forward(0.1);
      sense('A');
    }
    AFTER_RIGHT: 
    sense('F'); // check for stop boolean
        if (frontDist < 20) {
          stop1 = true;
        }
    turn_right(ANGLE);
    check_too_close('L', true);
//    sense('A');
    sense('F');
    while (frontDist > 20) {
      compare_align(true, true);
      for(int i = 0; i<25; i++) {
        if (frontDist>20 && rightDist1 > 15) {
          move_forward(0.1);
          if (stop2) { // once stop2 is set false, never check again / by default it is true
            if (leftDist1<20) {
              stop2 = true;
            } else {
              stop2 = false;
            }
          }
        }
        else  {
          stop_moving(0.5);
          break;
        }
        sense('A');
      }
      sense('A'); 
      if (rightDist1 < 15) { // has right obstacle (obstacle in middle of the room)
        Serial.println("Obstable mode right ");
        obstacle_mode = true; // going round the obstacle
        align_right(true);
        move_forward(0.5);
        goto RIGHT;
      }
    }
    
    if (obstacle_mode) { // end of obstacle mode
      Serial.println("End of obstacle mode ");
      turn_right(ANGLE);
      obstacle_mode = false;
      move_forward(0.5);
      goto AFTER_RIGHT;
    }
    // no right obstacle in the middle of the room and has front obstacle 
    turn_left(ANGLE);
    sense ('F');
    if (frontDist > 20) {
      stop2 = false;
    }
    if (stop1&&stop2) {
      //frontDist<15
      goto STOP;
    } else {
      stop1 = false;
      stop2 = true;
    }
    align_right(true);
    move_forward(0.5);
    goto AFTER_LEFT;
  } else if (rightDist1 < 30) {
    while (rightDist1 < 30) {
      
    RIGHT:  
      align_right(true);
      
      if (!obstacle_mode){ 
        check_too_close('R', true);
      }
      sense('F');
      for(int i = 0; i<25; i++) {
        if (obstacle_mode) {
          Serial.println("Going first edge ");
          sense('A');
          while (frontDist>20 && (rightDist1<15 || rightDist1==0)) { // going along first edge of the obstacle on the right
            move_forward(0.1);
            sense('A');
          }
          break; // end of first edge
        }
        else if (frontDist>20) { // not obstacle mode
            move_forward(0.1);
        }
        else break;
        sense('F');
      }
      if (frontDist < 20) { // meet front wall
        
        turn_left(ANGLE);
        align_right(true);
        move_forward(0.5);
        check_too_close('R', true);
        move_forward(0.2);
        
        goto AFTER_LEFT;
      }
    }
    // went one side of the right obstacle, go to the other side (no more right obstacle)
    turn_right(ANGLE);
    move_forward(0.5);
    align_right(true);
    sense('A');
    while (rightDist1 < 15 && frontDist > 20) { // keep going along the second edge
      Serial.println("Going second edge ");
      move_forward(0.1);
      sense('A');
    }
    AFTER_LEFT: 
    sense('F');
        if (frontDist < 20) {
          stop1 = true;
        }
    turn_left(ANGLE); // continue path - end of second edge
    check_too_close('R', true);
    sense('F');
    while(frontDist > 20) { 
      compare_align(true, true);
      for(int i = 0; i<25; i++) {
        if (frontDist>20 && leftDist1>15) { // moving towards the wall
          move_forward(0.1);
          if (stop2) { // once stop2 is set false, never check again
            if (rightDist1<20) {
              stop2 = true;
            } else {
              stop2 = false;
            }
          }
        }
        else  {
          stop_moving(0.5);
          break;
        }
        sense('A');
      }
      sense('A');
      if (leftDist1 < 15) { // has left obstacle in middle of the room, go round it
        Serial.println("Obstacle mode left ");
        obstacle_mode = true;
        align_left(false);
        move_forward(0.5);
        goto LEFT;
      }
    }
    if (obstacle_mode) { // end of obstacle mode
      Serial.println("End of obstacle mode ");
      turn_left(ANGLE);
      obstacle_mode = false;
      move_forward(1);
      goto AFTER_LEFT;
    }
    // not obstacle mode, meet front wall
    turn_right(ANGLE);
    move_forward(1);
    sense ('F');
    if (frontDist > 20) {
      stop2 = false;
    } 
    if (stop1&&stop2) {
      goto STOP;
    } else {
      stop1 = false;
      stop2 = true;
    }
    align_left(true);
    move_forward(0.5);
    goto AFTER_RIGHT;
  }
  STOP: stopie();
}

void check_too_close(char side, bool compensate) { // prevent the robot from getting too close to the wall which may not allow it to make a turn
  Serial.println("Check too close");
  if (side == 'L') { // check on left side
    sense('L');
    if (compensate) move_backward(0.5);
    while (leftDist1 < 10) {
      turn_left(0.3); // first turn a little bit toward the wall
      move_backward(0.3); 
      turn_right(0.4); // then turn to the opposite side 
      move_forward(0.3); // now the robot has gotten further from the wall
      align_left(true); // align with the wall
      sense('L'); // update distances
      if (compensate) move_backward(0.5);
    }
  } else if (side == 'R') { // check on right side
    sense('R');
    if (compensate) move_backward(0.5);
    while (rightDist1 < 10) { // similar mechanism to left side
      turn_right(0.3);
      move_backward(0.3);
      turn_left(0.4);
      move_forward(0.3);
      align_right(true);
      sense('R');
      if (compensate) move_backward(0.5);
    }
  }
}

void loop() { // this function will be called repeatedly
  process_command();
//coverage_algorithm();
}

void process_command() { // communications with the application
    if (esp8266.available()) {// check if the esp is sending a message
      if (esp8266.find("+IPD,")) { // check if a web browser is connecting to ESP
          delay(100); // wait for the serial buffer to fill up (read all the serial data)
          // get the connection id so that we can then disconnect
          int connectionId = esp8266.read() - 48; // subtract 48 because the read() function returns
        
          // the ASCII decimal value and 0 (the first decimal number) starts at 48
          // advance cursor to "pin="
          esp8266.find("pin=");
          int pinNumber = (esp8266.read() - 48); // get first number i.e. if the pin 13 then the 1st number is 1, then multiply to get 10
                
          Serial.println(pinNumber);
          
          esp8266.flush();
          if (pinNumber == TURN_LEFT) {
              left();
          }

          else if (pinNumber == TURN_RIGHT) {
            right();
          }

          else if (pinNumber == MOVE_FORWARD) {
            forward();
          } 

          else if (pinNumber == MOVE_BACKWARD) {
            backward();
          }

          else if (pinNumber == STOP_MOVING) {
            stopie();
          }

          else if (pinNumber == START_ALGORITHM) {
            coverage_algorithm(); 
          }
          //make close command
          count ++;
          Serial.print("Count :");
          Serial.println(count);
          if(count ==5) {
            count = 0;
            esp8266.flush();
            sendData("AT+RST\r\n", 2000, DEBUG); // reset module
            sendData("AT+CIPMUX=1\r\n", 1000, DEBUG); // configure for multiple 
            sendData("AT+CIPSERVER=1,6543\r\n", 1000, DEBUG); // turn on server on port 6543

          }
      }
    }
}

String sendData(String command, const int timeout, boolean debug) {
    String response = "";
    esp8266.print(command); // send the read character to the esp8266
    
    long int time = millis();
    while ((time+timeout) > millis()) {
        while(esp8266.available()) {
    // The esp has data so display its output to the serial window
            char c = esp8266.read(); // read the next character.
            response+=c;
        }
    }
    
    if(debug) {
      Serial.print(response);
    }
    return response;
}
