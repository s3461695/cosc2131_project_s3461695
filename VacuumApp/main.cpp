#include "mainscreen.h"
#include <QApplication>
#include <QtNetwork/QNetworkInterface>
#include <QPixmap>
#include <QSplashScreen>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainScreen w;
    w.setStyleSheet("background-color: #669999;");
    w.show();

    return a.exec();

}


