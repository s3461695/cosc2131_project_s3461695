#ifndef AUTOMODE_H
#define AUTOMODE_H

#include <QWidget>
#include <QStackedWidget>

namespace Ui {
class AutoMode;
}

class AutoMode : public QWidget
{
    Q_OBJECT

public:
    explicit AutoMode(QStackedWidget *s, QWidget *parent = 0);
    ~AutoMode();

private slots:
    void on_start_button_clicked();


private:
    QStackedWidget *screens;
    Ui::AutoMode *ui;

};

#endif // AUTOMODE_H
