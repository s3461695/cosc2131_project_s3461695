#include "mode.h"
#include "ui_mode.h"
#include "automode.h"
#include "ui_automode.h"


Mode::Mode( QStackedWidget *s, QWidget *parent) :
    screens(s),
    QDialog(parent),

    ui(new Ui::Mode)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color: #669999;");
    ui->ok_button->setStyleSheet("background-color: #526F6F;");
}

Mode::~Mode()
{
    delete ui;
}

void Mode::on_ok_button_clicked()
{
    if (ui->auto_mode->isChecked()) {
        screens->setCurrentIndex(1);
    } else if (ui->mobile_mode->isChecked()) {
        screens->setCurrentIndex(2);
    }
}
