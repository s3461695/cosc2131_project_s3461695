#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <QMainWindow>
#include <QStackedWidget>

namespace Ui {
class MainScreen;
}

class MainScreen : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainScreen(QWidget *parent = 0);
    ~MainScreen();

private slots:

    void on_actionAbout_triggered();

    void on_actionQuit_triggered();

    void on_actionBack_triggered();


private:
    Ui::MainScreen *ui;
    QStackedWidget *screens;
};

#endif // MAINSCREEN_H
