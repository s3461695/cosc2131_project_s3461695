#-------------------------------------------------
#
# Project created by QtCreator 2015-12-23T13:15:43
#
#-------------------------------------------------

QT       += core gui
QT += network
QT+=uitools

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VacuumApp
TEMPLATE = app


SOURCES += main.cpp\
    mode.cpp \
    automode.cpp \
    mainscreen.cpp \
    mobilemode.cpp

HEADERS  += \
    mode.h \
    automode.h \
    mainscreen.h \
    mobilemode.h

FORMS    += \
    mode.ui \
    automode.ui \
    mainscreen.ui \
    mobilemode.ui

CONFIG += mobility

MOBILITY = 

DISTFILES +=

RESOURCES += \
    resources.qrc

