#ifndef MOBILEMODE_H
#define MOBILEMODE_H

#include <QWidget>
#include <QStackedWidget>

namespace Ui {
class MobileMode;
}

class MobileMode : public QWidget
{
    Q_OBJECT

public:
    explicit MobileMode(QStackedWidget *s, QWidget *parent = 0);
    ~MobileMode();

private slots:

    void on_up_clicked();

    void on_right_clicked();

    void on_down_clicked();

    void on_left_clicked();

    void on_stop_clicked();

    void send_command(QString s);

private:
    QStackedWidget *screens;
    Ui::MobileMode *ui;
};

#endif // MOBILEMODE_H
