#include "automode.h"
#include "ui_automode.h"
#include <QCoreApplication>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <QObject>

AutoMode::AutoMode(QStackedWidget *s, QWidget *parent) :
    screens(s),
    QWidget(parent),
    ui(new Ui::AutoMode)
{
    ui->setupUi(this);
    ui->start_button->setStyleSheet("background-color: #526F6F;");
}

AutoMode::~AutoMode()
{
    delete ui;
}


void AutoMode::on_start_button_clicked()
{

    QEventLoop eventLoop;
    QNetworkAccessManager *netManager = new QNetworkAccessManager(this);
    QUrl url("http://192.168.4.1:6543?pin=6");
    QNetworkRequest req(url);

    QNetworkReply *reply = netManager->get(req);

    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();

}

