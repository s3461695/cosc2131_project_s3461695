#include "mobilemode.h"
#include "ui_mobilemode.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>

bool keep_going = false;


MobileMode::MobileMode(QStackedWidget *s, QWidget *parent) :
    QWidget(parent),
    screens(s),
    ui(new Ui::MobileMode)
{
    ui->setupUi(this);
    QPixmap pixmap_down(":/down.png");
    ui->down->setIcon(QIcon(pixmap_down));
    ui->down->setIconSize(QSize(100, 100));

    QPixmap pixmap_up(":/up.png");
    ui->up->setIcon(QIcon(pixmap_up));
    ui->up->setIconSize(QSize(100, 100));

    QPixmap pixmap_left(":/left.png");
    ui->left->setIcon(QIcon(pixmap_left));
    ui->left->setIconSize(QSize(100, 100));

    QPixmap pixmap_right(":/right.png");
    ui->right->setIcon(QIcon(pixmap_right));
    ui->right->setIconSize(QSize(100, 100));

    QPixmap pixmap_stop(":/stop.png");
    ui->stop->setIcon(QIcon(pixmap_stop));
    ui->stop->setIconSize(QSize(100, 100));
}

MobileMode::~MobileMode()
{
    delete ui;
}

void MobileMode::send_command(QString s) {
    QEventLoop eventLoop;
    QNetworkAccessManager *netManager = new QNetworkAccessManager(this);
    QUrl url(s);
    QNetworkRequest req(url);

    QNetworkReply *reply = netManager->get(req);

    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
}

void MobileMode::on_up_clicked()
{
    send_command("http://192.168.4.1:6543?pin=3");
}

void MobileMode::on_right_clicked()
{
    send_command("http://192.168.4.1:6543?pin=2");
}

void MobileMode::on_down_clicked()
{
    send_command("http://192.168.4.1:6543?pin=4");
}

void MobileMode::on_left_clicked()
{
    send_command("http://192.168.4.1:6543?pin=1");
}

void MobileMode::on_stop_clicked()
{
    send_command("http://192.168.4.1:6543?pin=5");
}
