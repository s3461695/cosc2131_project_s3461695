#ifndef MODE_H
#define MODE_H

#include <QDialog>
#include <QStackedWidget>

namespace Ui {
class Mode;
}

class Mode : public QDialog
{
    Q_OBJECT

public:
    explicit Mode(QStackedWidget* s, QWidget *parent = 0);
    ~Mode();

private slots:
    void on_ok_button_clicked();

private:
    QStackedWidget* screens;
    Ui::Mode *ui;

};

#endif // MODE_H
