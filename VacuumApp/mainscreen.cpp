#include "mainscreen.h"
#include "ui_mainscreen.h"
#include "mode.h"
#include "automode.h"
#include "mobilemode.h"

MainScreen::MainScreen(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainScreen)
{
    ui->setupUi(this);
    ui->menubar->setStyleSheet("background-color: #407F7F;");
    ui->menuOptions->setStyleSheet("background-color: #728888;");
    screens = new QStackedWidget;
    Mode *md = new Mode(screens);
    AutoMode *am = new AutoMode(screens);
    MobileMode *mm = new MobileMode(screens);
    screens->addWidget(md);
    screens->addWidget(am);
    screens->addWidget(mm);

    this->setCentralWidget(screens);
}

MainScreen::~MainScreen()
{
    delete ui;
}

void MainScreen::on_actionAbout_triggered()
{
    QApplication::aboutQt();
}

void MainScreen::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainScreen::on_actionBack_triggered()
{
    if (screens->currentIndex() == 2) {
        screens->setCurrentIndex(0);
    } else if (screens->currentIndex() != 0){
        screens->setCurrentIndex(screens->currentIndex()-1);
    }
}
